﻿CREATE PROCEDURE [dbo].[uspSetUser]
	@Command VARCHAR(MAX),

	@StudentId NUMERIC(18,0),
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT

AS
	BEGIN

	IF @Command='UPDATE'
		BEGIN

			SELECT 
			@FirstName=CASE WHEN @FirstName IS NULL THEN US.FirstName ELSE @FirstName END,
			@LastName=CASE WHEN @LastName IS NULL THEN US.LastName ELSE @LastName END
				FROM tblUsers AS US
					WHERE US.StudentId=@StudentId

			UPDATE tblUsers
				SET FirstName=@FirstName,
				LastName=@LastName
					WHERE StudentId=@StudentId
					

			SET @Status=1
			SET @Message='UPDATE SUCCESSFULL'
			
		END

	ELSE IF @Command='DELETE'
		BEGIN

			DELETE FROM tblUsers
				WHERE StudentId=@StudentId

			SET @Status=1
			SET @Message='DELETE SUCCESSFULL'

		END

	END

GO
