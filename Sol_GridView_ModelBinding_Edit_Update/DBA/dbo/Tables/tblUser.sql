﻿CREATE TABLE [dbo].[tblUser] (
    [UserId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [Age]       INT          NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC)
);

