﻿CREATE TABLE [dbo].[tblUsers]
(
	[StudentId] NUMERIC NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [FirstName] VARCHAR(50) NOT NULL, 
    [LastName] VARCHAR(50) NOT NULL
)
