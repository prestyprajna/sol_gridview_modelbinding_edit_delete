﻿using Sol_GridView_ModelBinding_Edit_Update.Dal.ORD;
using Sol_GridView_ModelBinding_Edit_Update.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_GridView_ModelBinding_Edit_Update.Dal
{
    public class PersonDal
    {
        #region  declaration
        private PersonDCDataContext _dc = null;
        #endregion

        #region   constructor
        public PersonDal()
        {
            _dc = new PersonDCDataContext();
        }

        #endregion

        #region   public methods

        private async Task<List<PersonEntity>> GetPersonDataAsync()
        {
            return await Task.Run(() =>
            {
                var getData =
                _dc?.tblUsers
                ?.AsEnumerable()
                ?.Select((lePersonObj) => new PersonEntity()
                {
                    StudentId = lePersonObj?.StudentId,
                    FirstName = lePersonObj?.FirstName,
                    LastName = lePersonObj?.LastName
                })
                ?.ToList();

                return getData;

            });
        }

        public async Task<int?> GetCountPersonData()
        {
            return await Task.Run(async() =>
            {
                return (await this.GetPersonDataAsync())
               ?.AsEnumerable()
               ?.Count();
            });
           
                
        }

        public async Task<List<PersonEntity>> GetPaginationData(int startRowIndex, int maximumRows)
        {
            return await Task.Run(async() =>
            {
                return (await this.GetPersonDataAsync())
                ?.AsEnumerable()
                ?.Skip(startRowIndex)
                ?.Take(maximumRows)
                ?.ToList();
            });
            
        }

        public async Task<PersonEntity> FindPersonData(int id)
        {
            return await Task.Run(async() =>
            {
                return (await this.GetPersonDataAsync())
                ?.AsEnumerable()
                ?.Where((lePersonEntityObj) => lePersonEntityObj.StudentId == id)
                ?.FirstOrDefault();
            });
        }

        public async Task<Boolean> UpdateData(PersonEntity personEntityObj)
        {
            return await Task.Run(() =>
            {
                int? status = null;
                string message = null;

                var updateData =
                _dc?.uspSetUser(
                    "UPDATE",
                    personEntityObj?.StudentId,
                    personEntityObj?.FirstName,
                    personEntityObj?.LastName,
                    ref status,
                    ref message
                    );

                return (status == 1) ? true : false;
            });
        }

        public async Task<Boolean> DeleteData(PersonEntity personEntityObj)
        {
            return await Task.Run(() =>
            {
                int? status = null;
                string message = null;

                var updateData =
                _dc?.uspSetUser(
                    "DELETE",
                    personEntityObj?.StudentId,
                    personEntityObj?.FirstName,
                    personEntityObj?.LastName,
                    ref status,
                    ref message
                    );

                return (status == 1) ? true : false;
            });
        }

        #endregion
    }
}