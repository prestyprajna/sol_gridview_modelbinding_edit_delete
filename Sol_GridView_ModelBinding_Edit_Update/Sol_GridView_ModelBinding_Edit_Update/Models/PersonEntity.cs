﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_GridView_ModelBinding_Edit_Update.Models
{
    public class PersonEntity
    {
        public decimal? StudentId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}