﻿using Sol_GridView_ModelBinding_Edit_Update.Dal;
using Sol_GridView_ModelBinding_Edit_Update.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_GridView_ModelBinding_Edit_Update
{
    public partial class Default : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            
        }

        #region  private methods

        public  async Task<SelectResult> BindPersonData(int startRowIndex,int maximumRows)
        {
            return await Task.Run(async() =>
            {
                int count = Convert.ToInt32(await new PersonDal()?.GetCountPersonData());

                List<PersonEntity> personEntityObj = await new PersonDal().GetPaginationData(startRowIndex, maximumRows);

                return new SelectResult(count, personEntityObj);
            });
        }


        #endregion

        // The id parameter name should match the DataKeyNames value set on the control
        public async Task gvPerson_UpdateItem(int id)
        {
            Sol_GridView_ModelBinding_Edit_Update.Models.PersonEntity item = null;
            // Load the item here, e.g. item = MyDataLayer.Find(id);

            item = await new PersonDal().FindPersonData(id);

            if (item == null)
            {
                // The item wasn't found
                ModelState.AddModelError("", String.Format("Item with id {0} was not found", id));
                return;
            }
            TryUpdateModel(item);
            if (ModelState.IsValid)
            {
                // Save changes here, e.g. MyDataLayer.SaveChanges();
               await new PersonDal().UpdateData(item);

            }
        }

        // The id parameter name should match the DataKeyNames value set on the control
        public async Task gvPerson_DeleteItem(PersonEntity personEntityObj)
        {
            await new PersonDal().DeleteData(personEntityObj);
        }
    }
}
//int id